﻿using System;
using F2B.Orchard.Schedule.Models;

namespace F2B.Orchard.Schedule.Providers
{
    public static class SchedulePartExtensions
    {
        public static int DaysIncluded(this SchedulePart part)
        {
            if (part.AllDay)
            {
                return part.Duration.Days;
            }
            //var end = part.StartDate + part.StartTime + part.Duration;
            var end = part.StartTime + part.Duration;
            var days = end.Days;
            if ((end - TimeSpan.FromDays(days)).TotalMinutes > 0) days++;
            return days;
        }
    }
}